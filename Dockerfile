FROM python:3.7.5-slim
RUN mkdir /app
WORKDIR /app
RUN python -m pip install flask
RUN apt update && apt install curl -y
COPY  ./app/main.py .
CMD ["python", "main.py"]
EXPOSE 8080
HEALTHCHECK CMD curl --fail http://localhost:8080 || exit 1

# Hello World

Simple hello world with docker and python

## Installation

Docker Installation

```bash
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

$ sudo apt-get update

$ sudo apt-get install docker-ce docker-ce-cli containerd.io

$ sudo usermod -aG docker your_username
for example:
$ sudo usermod -aG docker ubuntu
```

Docker Compose Installation

```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

Hello App

```bash
$ git clone https://gitlab.com/faizal1801/hello.git
$ cd hello
$ docker-compose up -d --build --force-recreate

```

## Testing

Open browser and access http://your_server_ip

